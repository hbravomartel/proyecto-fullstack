import { validarToken } from "../services/token.service"
import * as httpStatusCode from "http-status-codes"

const authentication = (req, res, next) => {
	const authorization = req.headers.authorization
	console.log(req.headers)

	if (authorization && authorization.split(" ").length > 0) {
		const token = authorization.split(" ")[1]

		console.log(token)

		validarToken(token)
			.then(payload => {
				res.locals.nombres = payload.nombres
				next()
			})
			.catch(error => {
				res.status(httpStatusCode.FORBIDDEN).send("Token inválido")
			})

	} else {
		res.status(httpStatusCode.FORBIDDEN).send("Usuario no autorizado")
	}
}

export default authentication