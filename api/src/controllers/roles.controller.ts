import GenericController from "./generic.controller";
import { RolesModel } from '../models';

class RolesController extends GenericController {
	constructor() {
		super(RolesModel)
	}
}

export default RolesController