import GenericController from "./generic.controller";
import { RadiosModel } from '../models';
import * as httpStatusCodes from "http-status-codes"
var path = require('path');

class RadiosController extends GenericController {
	constructor() {
		super(RadiosModel)
	}

	async insert(req, res) {
		const data = req.body
		req.body.logo = path.parse(req.body.logo).base;
	
		const radio = new RadiosModel(data)
		await radio.save()

		res
			.status(httpStatusCodes.CREATED)
			.json({
				status: httpStatusCodes.CREATED,
				message: "Radio ingresada"
			})
	}

	async update(req, res) {
		const data = req.body

		if(req.body.logo)
		req.body.logo = path.parse(req.body.logo).base;
		
		await RadiosModel.findOneAndUpdate({ _id: req.params.id }, data)

		res
			.status(httpStatusCodes.OK)
			.json({
				status: httpStatusCodes.OK,
				message: "Radio actualizada"
			})
	}

	async upload(req, res)
	{
		const data = req.body
		res
			.status(httpStatusCodes.OK)
			.json(data.foto)
	}

}

export default RadiosController