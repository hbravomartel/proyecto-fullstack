import GenericController from "./generic.controller";
import { GenerosModel } from '../models';

class GenerosController extends GenericController {
	constructor() {
		super(GenerosModel)
	}
}

export default GenerosController