import GenericController from "./generic.controller";
import { PaisesModel } from '../models';

class GenerosController extends GenericController {
	constructor() {
		super(PaisesModel)
	}
}

export default GenerosController