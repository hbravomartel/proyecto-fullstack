import * as express from "express"
import { PaisesController } from "../controllers"

const route = express.Router()
const controller = new PaisesController()

route.get("/", controller.getAll)

export default route