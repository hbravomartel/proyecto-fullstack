import * as express from "express"
import { RadiosController } from "../controllers"
import { authentication } from '../policies';
import { errorsHandler } from "../handlers/errors.handler";

const route = express.Router()
const controller = new RadiosController()

route.get("/", controller.getAll)
route.get("/:id", controller.getOne)
route.post("/", authentication, errorsHandler.catchAsync(controller.insert))
route.put("/:id", controller.update)
route.delete("/:id", controller.delete)

export default route