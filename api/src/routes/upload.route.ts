import * as express from "express"
import { RadiosController } from "../controllers"
import { errorsHandler } from "../handlers/errors.handler";
import { filesHandler } from "../handlers/files.handler";
import { STORAGE } from "../enums/storage.enum";
import { TYPE } from "../enums/type.enum";

const route = express.Router()
const controller = new RadiosController()

route.post("/",  filesHandler.upload({ storage: STORAGE.DISK, type: TYPE.SINGLE, fields: "imagen" }), errorsHandler.catchAsync(controller.upload))



export default route