import * as http from "http"
import * as express from "express"
import { usuarioRoute, radiosRoute, generosRoute, rolesRoute, paisesRoute,uploadRoute } from '../routes';
import * as bodyParser from "body-parser"
import * as path from "path"

const app = express()

const initializeServer = () => {
	return new Promise((resolve, reject) => {
		const server = http.createServer(app)

		app.use(bodyParser.json())
		app.use(bodyParser.urlencoded({ extended: true }))

		app.use(express.static("public"))

		app.use(function (req, res, next) {
			res.setHeader('Access-Control-Allow-Origin', '*');
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
			res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

			next();
			});

		app.use("/usuarios", usuarioRoute)
		app.use("/radios", radiosRoute)
		app.use("/generos", generosRoute)
		app.use("/roles", rolesRoute)
		app.use("/paises", paisesRoute)	
		app.use("/upload", uploadRoute)	

		app.use("**", (req, res, next) => {
			res.status(404).send("Path not found")
		})

		server
			.listen(3000, () => console.log("servidor ejecutándose en el puerto 4000"))
			.on("listening", () => resolve())
			.on("error", (error) => reject(error))
	})
}

export default initializeServer