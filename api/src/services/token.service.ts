import * as moment from "moment"
import * as jwt from "jwt-simple"

const KEYWORD_SECRET = "f9e77f69-a72b-48f7-ba79"

const crearToken = (_id, nombres, apellidos) => {
	const payload = {
		_id,
		nombres,
		apellidos,
		iat: moment().unix(),
		exp: moment().add(3, "hours").unix()
	}

	const token = jwt.encode(payload, KEYWORD_SECRET)
	console.log(token)
	return token
}

const validarToken = (token): Promise<any> => {
	return new Promise((resolve, reject) => {
	
		try {
			const payload = jwt.decode(token, KEYWORD_SECRET)
			console.log(payload)
			resolve(payload)
		} catch (error) {
			reject(error)
		}
	})
}

export { crearToken, validarToken }