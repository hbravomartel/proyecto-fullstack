const mongoose = require("mongoose")

const schema = new mongoose.Schema({
    nombre: {
		type: String,
		size: 255,
		required: true
    },
    fecha_registro: {
        type: Date,
        defaultTo: new Date()
    }
    
})

const model = mongoose.model("paises", schema)

export default model