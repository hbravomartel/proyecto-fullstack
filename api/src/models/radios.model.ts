const mongoose = require("mongoose")

const schema = new mongoose.Schema({
    nombre: {
		type: String,
		size: 255,
		required: true
    },
    descripcion: String,
    stream: {
		type: String,
		size: 255,
		required: true
    },
    website:  String,
    logo:  
    {
        type: String,
        default: 'default.png'
    },
    fecha_registro: {
        type: Date,
        default: new Date()
    },
    estado: {
        type: Number,
        default: 1
    },
    paises: {
        type: mongoose.Schema.ObjectId,
        ref: "paises"
    },
    generos: {
        type: mongoose.Schema.ObjectId,
        ref: "generos"
    }
})

function autoPoblar(next) {
    this.populate("generos")
    this.populate("paises")
    next()
}


schema.pre("find", autoPoblar)
const model = mongoose.model("radios", schema)

export default model