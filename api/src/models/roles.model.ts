import mongoose = require("mongoose")

const schema = new mongoose.Schema({
	nombre: {
		type: String,
		required: true,
		trim: true,
		unique: true
	},
	estado: Number
})

const model = mongoose.model("roles", schema)

export default model