const mongoose = require("mongoose")

const schema = new mongoose.Schema({
    nombre: {
		type: String,
		size: 255,
		required: true
    },
    imagen: String,
    fecha_registro: {
        type: Date,
        defaultTo: new Date()
    },

    
})

const model = mongoose.model("generos", schema)

export default model