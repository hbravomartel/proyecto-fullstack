const mongoose = require("mongoose")

const scheme = new mongoose.Schema({
	nombres: {
		type: String,
		size: 255,
		required: true
	},
	apellidos: {
		type: String,
		size: 255,
		required: true
	},
	email: {
		type: String,
		required: true,
		unique: true,
		email: true,
		lowercase: true,
		trim: true
	},
	password: {
		type: String,
		size: 255,
		required: true
	},
	estado: {
		type: Number,
		defaultTo: 1
	},	
	roles: 
		{
			type: mongoose.Schema.ObjectId,
			ref: "roles"
		},	
	fecha_registro: {
		type: Date,
		default: new Date()
	}
})

function autoPoblar(next) {
	this.populate("roles")
	next()
}

scheme.pre("find", autoPoblar)
scheme.pre("findOne", autoPoblar)

const model = mongoose.model("usuarios", scheme)

export default model